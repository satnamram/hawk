package hawk

import (
	"testing"
	"bytes"
	"encoding/json"
	"time"
	"math/rand"
)


type testAPI struct {
	*TestModule
}

type TestModule struct {
	counter         int
	getterSource    *testObject
	setterTarget    *testObject
	setterTargetRaw []byte
	getterSourceRaw []byte
}

func (t *TestModule) SetterNoGetterRaw(s Session, input []byte) error {
	t.setterTargetRaw = input
	return nil
}

func (t *TestModule) NoSetterGetterRaw(s Session) ([]byte, error) {
	return t.getterSourceRaw, nil
}

func (t *TestModule) SetterGetterRawFull(s Session, input []byte) ([]byte, error) {
	return input, nil
}

func (t *TestModule) SetterGetterRawOutput(s Session, input *testObject) ([]byte, error) {
	return json.Marshal(input)
}

func (t *TestModule) SetterGetterRawInput(s Session, input []byte) (*testObject, error) {
	obj := &testObject{}
	err := json.Unmarshal(input, obj)
	if err != nil {
		return nil, err
	}
	return obj, nil
}

func (t *TestModule) NoSetterNoGetter(s Session) error {
	t.counter++
	return nil
}

func (t *TestModule) SetterNoGetter(s Session, input *testObject) error {
	t.setterTarget = input
	return nil
}

func (t *TestModule) NoSetterGetter(s Session) (*testObject, error) {
	return t.getterSource, nil
}

func (t *TestModule) SetterGetter(s Session, input *testObject) (*testObject, error) {
	return input, nil
}

func createTestModule(t *testing.T) (*serverAPI, *TestModule) {
	rand.Seed(time.Now().UTC().UnixNano())
	mod := &TestModule{}
	api, err := newServerAPI(&testAPI{TestModule: mod})
	if err != nil {
		t.Fatal(err)
	}
	return api, mod
}

type testObject struct {
	Int    int
	String string
	Bytes  []byte
	Time   time.Time
	Bool   bool
}

func (p *testObject) toJSON() []byte {
	b, err := json.Marshal(p)
	if err != nil {
		panic(err)
	}
	return b
}

func (o1 *testObject) mustEqual(o2 *testObject, t *testing.T) {
	if o1.Int != o2.Int {
		t.Fatal("Int field not equal")
	}
	if o1.String != o2.String {
		t.Fatal("String field not equal")
	}
	if !bytes.Equal(o1.Bytes, o2.Bytes) {
		t.Fatal("Bytes field not equal")
	}
	if !o1.Time.Equal(o2.Time) {
		t.Fatal("Time field not equal")
	}
	if o1.Bool != o2.Bool {
		t.Fatal("Bool field not equal")
	}
}

func createTestObjects(n int) []*testObject {
	ret := make([]*testObject, n)
	for i := 0; i < n; i++ {
		ret[i] = createTestObject()
	}
	return ret
}

func parseTestObject(b []byte) *testObject {
	obj := &testObject{}
	err := json.Unmarshal(b, obj)
	if err != nil {
		panic(err)
	}
	return obj
}

func createTestObject() *testObject {
	o := &testObject{}
	o.Int = randomInt(0, 99999)
	o.String = randomString(100)
	o.Bytes = []byte(randomString(100))
	o.Time = time.Now()
	o.Bool = true
	return o
}

func randomString(l int) string {
	bytes := make([]byte, l)
	for i := 0; i < l; i++ {
		bytes[i] = byte(randomInt(65, 90))
	}
	return string(bytes)
}

func randomInt(min int, max int) int {
	return min + rand.Intn(max-min)
}

func TestResolveSetterNoGetterRaw(t *testing.T) {
	mod := &TestModule{}
	f, exist := resolveMethod(mod.SetterNoGetterRaw)
	if exist {
		raw := []byte("PAYLOAD A")
		ret, err := f(&nopSession{}, raw)
		if err != nil {
			t.Fatal(err)
		}
		if ret != nil {
			t.Fail()
		}
		if !bytes.Equal(mod.setterTargetRaw, raw) {
			t.Fail()
		}
	} else {
		t.Fail()
	}
}

func TestResolveNoSetterGetterRaw(t *testing.T) {
	mod := &TestModule{}
	f, exist := resolveMethod(mod.NoSetterGetterRaw)
	if exist {
		mod.getterSourceRaw = []byte("PAYLOAD B")
		ret, err := f(&nopSession{}, nil)
		if err != nil {
			t.Fatal(err)
		}
		if ret == nil {
			t.Fail()
		}
		if !bytes.Equal(ret, mod.getterSourceRaw) {
			t.Fail()
		}
	} else {
		t.Fail()
	}
}

func TestResolveSetterGetterRawFull(t *testing.T) {
	mod := &TestModule{}
	f, exist := resolveMethod(mod.SetterGetterRawFull)
	if exist {
		raw := []byte("PAYLOAD C")
		ret, err := f(&nopSession{}, raw)
		if err != nil {
			t.Fatal(err)
		}
		if ret == nil {
			t.Fail()
		}
		if !bytes.Equal(raw, ret) {
			t.Fail()
		}
	} else {
		t.Fail()
	}
}

func TestResolveSetterGetterRawInput(t *testing.T) {
	mod := &TestModule{}
	f, exist := resolveMethod(mod.SetterGetterRawInput)
	if exist {
		obj := createTestObject()
		raw, err := json.Marshal(obj)
		if err != nil {
			t.Fatal(err)
		}
		ret, err := f(&nopSession{}, raw)
		if err != nil {
			t.Fatal(err)
		}
		if ret == nil {
			t.Fail()
		}
		parseTestObject(ret).mustEqual(obj, t)
	} else {
		t.Fail()
	}
}

func TestResolveSetterGetterRawOutput(t *testing.T) {
	mod := &TestModule{}
	f, exist := resolveMethod(mod.SetterGetterRawOutput)
	if exist {
		obj := createTestObject()
		ret, err := f(&nopSession{}, obj.toJSON())
		if err != nil {
			t.Fatal(err)
		}
		if ret == nil {
			t.Fail()
		}
		parseTestObject(ret).mustEqual(obj, t)
	} else {
		t.Fail()
	}
}

func TestResolveNoSetterNoGetter(t *testing.T) {
	mod := &TestModule{}
	f, exist := resolveMethod(mod.NoSetterNoGetter)
	if exist {
		if mod.counter != 0 {
			t.Fail()
		}
		ret, err := f(&nopSession{}, nil)
		if err != nil {
			t.Fatal(err)
		}
		if ret != nil {
			t.Fail()
		}
		if mod.counter != 1 {
			t.Fail()
		}
	} else {
		t.Fail()
	}
}

func TestResolveSetterNoGetter(t *testing.T) {
	mod := &TestModule{}
	f, exist := resolveMethod(mod.SetterNoGetter)
	if exist {
		obj := createTestObject()
		ret, err := f(&nopSession{}, obj.toJSON())
		if err != nil {
			t.Fatal(err)
		}
		if ret != nil {
			t.Fail()
		}
		mod.setterTarget.mustEqual(obj, t)
	} else {
		t.Fail()
	}
}

func TestResolveNoSetterGetter(t *testing.T) {
	mod := &TestModule{}
	f, exist := resolveMethod(mod.NoSetterGetter)
	if exist {
		mod.getterSource = createTestObject()
		ret, err := f(&nopSession{}, nil)
		if err != nil {
			t.Fatal(err)
		}
		if ret == nil {
			t.Fail()
		}
		parseTestObject(ret).mustEqual(mod.getterSource, t)
	} else {
		t.Fail()
	}
}

func TestResolveSetterGetter(t *testing.T) {
	mod := &TestModule{}
	f, exist := resolveMethod(mod.SetterGetter)
	if exist {
		obj := createTestObject()
		ret, err := f(&nopSession{}, obj.toJSON())
		if err != nil {
			t.Fatal(err)
		}
		if ret == nil {
			t.Fail()
		}
		parseTestObject(ret).mustEqual(obj, t)
	} else {
		t.Fail()
	}
}

type nopSession struct{}

func (s *nopSession) Name() string {
	return ""
}

func (s *nopSession) Addr() string {
	return ""
}

func (s *nopSession) Meta(key string) interface{} {
	return nil
}

func (s *nopSession) Close() error {
	return nil
}
