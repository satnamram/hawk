package hawk

import (
	"errors"
	"reflect"
	"unicode"
)

var rawDataType = reflect.TypeOf([]byte{})

type serverAPI struct {
	Modules []string
	Methods map[string]func(Session, []byte) ([]byte, error)
}

func newServerAPI(v interface{}) (*serverAPI, error) {
	api := &serverAPI{
		Methods: make(map[string]func(Session, []byte) ([]byte, error)),
	}

	// if api interface was passed register all fields as new Modules
	if v != nil {
		vType := reflect.TypeOf(v).Elem()
		vValue := reflect.ValueOf(v).Elem()

		// iterate the fields of the given struct interface
		for i := 0; i < vType.NumField(); i++ {

			// do not accept zero values
			if vValue.Field(i).IsNil() {
				return nil, errors.New("serverAPI Module with zero value")
			}

			// prepare name
			vName := vType.Field(i).Name
			vNameRunes := []rune(vName)
			vNameRunes[0] = unicode.ToLower(vNameRunes[0])
			vName = string(vNameRunes)

			err := api.Register(vName, vValue.Field(i).Interface())
			if err != nil {
				return nil, err
			}
		}
	}
	return api, nil
}

func (api *serverAPI) Register(moduleName string, v interface{}) error {

	// check if pointer and get name
	vType := reflect.TypeOf(v)
	if vType.Kind() != reflect.Ptr {
		return errors.New("Can only Register Pointers.")
	}

	// check if module exists
	for _, existingModuleName := range api.Modules {
		if existingModuleName == moduleName {
			return errors.New("serverAPI Module \"" + moduleName + "\" does already exist")
		}
	}

	// reflect all Methods, collect valid ones
	for i := 0; i < vType.NumMethod(); i++ {

		// 1. Basic Information
		method := vType.Method(i)
		objectPtr := reflect.ValueOf(v)
		methodPtr := objectPtr.MethodByName(method.Name)
		methodNameRunes := []rune(method.Name)
		methodNameRunes[0] = unicode.ToLower(methodNameRunes[0])
		methodName := moduleName + "." + string(methodNameRunes)

		// 2. Method
		resolvedMethod, ok := resolveMethod(methodPtr.Interface())
		if ok {
			api.Methods[methodName] = resolvedMethod
			continue
		}
	}

	api.Modules = append(api.Modules, moduleName)
	return nil
}

// session interface and implementation
var apiSessionType = reflect.TypeOf((*Session)(nil)).Elem()

type Session interface {
	Name() string
	Addr() string
	Meta(key string) interface{}
	Close() error
}

type serverSession struct {
	info   *SessionInfo
	socket *Socket
}

func (session *serverSession) Name() string {
	return session.info.Name
}

func (session *serverSession) Addr() string {
	return session.info.Addr
}

func (session *serverSession) Meta(key string) interface{} {
	if value, exist := session.info.Meta[key]; exist {
		return value
	}
	return nil
}

func (session *serverSession) Close() error {
	return session.socket.Close()
}
