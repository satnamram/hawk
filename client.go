package hawk

import (
	"encoding/json"
	"errors"
	"fmt"
	"io"
	"sync"
	"bytes"
)

type Client struct {
	info *SessionInfo

	socket       *Socket
	socketWG     sync.WaitGroup
	socketClosed bool

	requests      map[uint32]chan *Message
	requestsSeq   uint32
	requestsMutex sync.Mutex
}

func NewClient(info *SessionInfo, conn io.ReadWriteCloser) *Client {
	c := &Client{
		info:     info,
		socket:   NewSocket(conn),
		requests: make(map[uint32]chan *Message),
	}

	// create response reader subroutine
	c.socketWG.Add(1)
	go func() {
		defer c.socketWG.Done()
		for {

			// read message
			msg, err := c.socket.Read()
			if err != nil {
				if !c.socketClosed {
					fmt.Println(err)
				}
				return // end subroutine
			}

			// dispatch request using message id
			// cleanup map entry and close channel
			c.requestsMutex.Lock()
			if returnChan, exist := c.requests[msg.ID]; exist {
				delete(c.requests, msg.ID)
				returnChan <- msg
			}
			c.requestsMutex.Unlock()
		}
	}()

	return c
}


func (c *Client) Call(command string, input interface{}) ([]byte, error) {

	// create message
	data, err := json.Marshal(input)
	if err != nil {
		return nil, err
	}
	m := &Message{
		Type:    Request,
		Command: command,
		Data:    data,
	}

	// set return channel and request id
	c.requestsMutex.Lock()
	m.ID = c.getUnusedID()
	retChan := make(chan *Message)
	c.requests[m.ID] = retChan
	c.requestsMutex.Unlock()

	// send message
	err = c.socket.Write(m)
	if err != nil {

		// error cleanup
		c.requestsMutex.Lock()
		close(c.requests[m.ID])
		delete(c.requests, m.ID)
		c.requestsMutex.Unlock()
		return nil, err
	}

	// wait for response
	resp := <-retChan

	// handle error
	if bytes.Equal(resp.Type, Error) {
		var errorMsg string
		err := json.Unmarshal(resp.Data, &errorMsg)
		if err != nil {
			return nil, err
		}
		return nil, errors.New(errorMsg)
	}

	return resp.Data, nil
}

func (c *Client) getUnusedID() uint32 {
	for {
		if _, exist := c.requests[c.requestsSeq]; !exist {
			return c.requestsSeq
		}
		c.requestsSeq++
	}
}

func (c *Client) Close() error {
	c.socketClosed = true
	err := c.socket.Close()
	c.socketWG.Wait()
	return err
}
