package hawk

import (
	"io"
	"encoding/binary"
	"errors"
	"bytes"
	"bufio"
)

type MessageType []byte

var (
	Request MessageType = []byte{0xf1}
	Success MessageType = []byte{0xf2}
	Error   MessageType = []byte{0xf3}
)

var commandDataDelimiter = []byte{0x0a}

type Message struct {
	Type MessageType
	ID   uint32
	Command string
	Data []byte
}

type Socket struct {
	writer *socketWriter
	reader *socketReader
	closer io.Closer
}

func NewSocket(conn io.ReadWriteCloser) *Socket {
	return &Socket{
		writer: newSocketWriter(conn),
		reader: newSocketReader(conn),
		closer: conn,
	}
}

func (s *Socket) Write(msg *Message) error {
	return s.writer.Write(msg)
}

func (s *Socket) Read() (*Message, error) {
	return s.reader.Read()
}

func (s *Socket) Close() error {
	return s.closer.Close()
}

type socketWriter struct {
	w   io.Writer
	buf bytes.Buffer
}

func newSocketWriter(w io.Writer) *socketWriter {
	return &socketWriter{w: w}
}

func (w *socketWriter) Write(msg *Message) error {
	commandLen := uint32(len(msg.Command))
	dataLen := uint32(len(msg.Data))
	if (commandLen+dataLen > 4294967295) {
		return errors.New("max message length exceeded")
	}
	defer w.buf.Reset()
	_, err := w.buf.Write(msg.Type)
	if err != nil {
		return err
	}
	err = binary.Write(&w.buf, binary.LittleEndian, msg.ID)
	if err != nil {
		return err
	}
	err = binary.Write(&w.buf, binary.LittleEndian, commandLen+dataLen+1)
	if err != nil {
		return err
	}
	_, err = w.buf.Write([]byte(msg.Command))
	if err != nil {
		return err
	}
	_, err = w.buf.Write(commandDataDelimiter)
	if err != nil {
		return err
	}
	_, err = w.buf.Write(msg.Data)
	if err != nil {
		return err
	}
	_, err = w.w.Write(w.buf.Bytes())
	if err != nil {
		return err
	}
	return nil
}

type socketReader struct {
	r io.Reader
}

func newSocketReader(r io.Reader) *socketReader {
	return &socketReader{r: bufio.NewReader(r)}
}

func (r *socketReader) Read() (*Message, error) {

	// wait for start byte
	var typeBuf [1]byte
	for !(typeBuf[0] >= 0xf1 && typeBuf[0] <= 0xf4) {
		n, err := io.ReadFull(r.r, typeBuf[:])
		if err != nil {
			return nil, err
		}
		if n == 0 {
			return nil, io.EOF
		}
	}

	msg := &Message{}

	// read type
	switch typeBuf[0] {
	case Request[0]:
		msg.Type = Request
	case Success[0]:
		msg.Type = Success
	case Error[0]:
		msg.Type = Error
	}

	// read id
	err := binary.Read(r.r, binary.LittleEndian, &msg.ID)
	if err != nil {
		return nil, err
	}

	// read lenght
	var l uint32
	err = binary.Read(r.r, binary.LittleEndian, &l)
	if err != nil {
		return nil, err
	}

	// create data buf and read bytes
	cmddata := make([]byte, l)
	_, err = io.ReadFull(r.r, cmddata)
	if err != nil {
		return nil, err
	}

	// read until delimiter
	for i := range cmddata {
		if cmddata[i] == commandDataDelimiter[0] {
			msg.Command = string(cmddata[:i])
			if uint32(i+1) < l {
				msg.Data = cmddata[i+1:]
			}
			return msg, nil
		}
	}

	return nil, errors.New("message without command data delimiter")
}
