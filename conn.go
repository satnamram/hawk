package hawk

import (
	"github.com/gorilla/websocket"
	"bytes"
	"io"
	"sync"
)

type Conn interface {
	ReadMessage() (messageType int, p []byte, err error)
	WriteMessage(messageType int, data []byte) error
	Close() error
}

type binaryConn struct {
	r    sync.Mutex
	w    sync.Mutex
	buf  bytes.Buffer
	conn Conn
}

func NewWS(conn Conn) io.ReadWriteCloser {
	return &binaryConn{conn: conn}
}

func (r *binaryConn) Read(b []byte) (int, error) {
	r.r.Lock()
	defer r.r.Unlock()
	if r.buf.Len() > 0 {
		return r.buf.Read(b)
	}
	for {
		t, msg, err := r.conn.ReadMessage()
		if err != nil {
			return 0, err
		}
		if t != websocket.BinaryMessage {
			continue;
		}
		r.buf.Write(msg);
		return r.buf.Read(b)
	}
}

func (w *binaryConn) Write(b []byte) (int, error) {
	w.w.Lock()
	defer w.w.Unlock()
	err := w.conn.WriteMessage(websocket.BinaryMessage, b)
	if err != nil {
		return 0, err
	}
	return len(b), nil
}

func (c *binaryConn) Close() error {
	return c.conn.Close()
}
