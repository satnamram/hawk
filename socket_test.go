package hawk

import (
	"testing"
	"bytes"
)

func TestSocket(t *testing.T) {
	var buf bytes.Buffer
	w := newSocketWriter(&buf)
	r := newSocketReader(&buf)

	m1 := &Message{
		Type:Request,
		Command:"hello.world",
		ID: 13,
		Data: []byte("PAYLOAD"),
	}

	err := w.Write(m1)
	if err != nil {
		t.Fatal(err)
	}

	m2, err := r.Read()
	if err != nil {
		t.Fatal(err)
	}

	if !equalMessage(m1, m2) {
		t.Fatal("message does not equal")
	}
}

func equalMessage(m1, m2 *Message) bool {
	if m1.Type[0] != m2.Type[0] {
		return false
	}
	if m1.Command != m2.Command {
		return false
	}
	if m1.ID != m2.ID {
		return false
	}
	if !bytes.Equal(m1.Data, m2.Data) {
		return false
	}
	return true
}