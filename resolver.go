package hawk

import (
	"reflect"
	"encoding/json"
)

type methodResolverFunc func(v interface{}) (func(Session, []byte) ([]byte, error), bool)

func resolveMethod(i interface {}) (func(Session, []byte) ([]byte, error), bool) {
	for _, resolver := range methodResolvers {
		f, ok := resolver(i)
		if ok {
			return f, true
		}
	}
	return nil, false
}

var methodResolvers = []methodResolverFunc{

	// SetterNoGetterRaw
	func(i interface{}) (func(Session, []byte) ([]byte, error), bool) {
		t := reflect.TypeOf(i)
		if t.NumIn() != 2 {
			return nil, false
		}
		if t.NumOut() != 1 {
			return nil, false
		}
		if !t.In(0).Implements(apiSessionType) {
			return nil, false
		}
		if t.In(1).String() != "[]uint8" {
			return nil, false
		}
		if t.Out(0).String() != "error" {
			return nil, false
		}
		v := reflect.ValueOf(i)
		return func(session Session, msg []byte) ([]byte, error) {
			result := v.Call([]reflect.Value{
				reflect.ValueOf(session),
				reflect.ValueOf(msg),
			})
			switch err := result[0].Interface().(type) {
			case error:
				return nil, err
			}
			return nil, nil
		}, true
	},

	// NoSetterGetterRaw
	func(i interface{}) (func(Session, []byte) ([]byte, error), bool) {
		t := reflect.TypeOf(i)
		if t.NumIn() != 1 {
			return nil, false
		}
		if t.NumOut() != 2 {
			return nil, false
		}
		if !t.In(0).Implements(apiSessionType) {
			return nil, false
		}
		if t.Out(0).String() != "[]uint8" {
			return nil, false
		}
		if t.Out(1).String() != "error" {
			return nil, false
		}
		v := reflect.ValueOf(i)
		return func(session Session, msg []byte) ([]byte, error) {
			result := v.Call([]reflect.Value{
				reflect.ValueOf(session),
			})
			switch err := result[1].Interface().(type) {
			case error:
				return nil, err
			}
			return result[0].Interface().([]byte), nil
		}, true
	},

	// SetterGetterRaw - full
	func(i interface{}) (func(Session, []byte) ([]byte, error), bool) {
		t := reflect.TypeOf(i)
		if t.NumIn() != 2 {
			return nil, false
		}
		if t.NumOut() != 2 {
			return nil, false
		}
		if !t.In(0).Implements(apiSessionType) {
			return nil, false
		}
		if t.In(1).String() != "[]uint8" {
			return nil, false
		}
		if t.Out(0).String() != "[]uint8" {
			return nil, false
		}
		if t.Out(1).String() != "error" {
			return nil, false
		}
		v := reflect.ValueOf(i)
		return func(session Session, msg []byte) ([]byte, error) {
			input := []reflect.Value{
				reflect.ValueOf(session),
				reflect.ValueOf(msg),
			}
			result := v.Call(input)
			switch err := result[1].Interface().(type) {
			case error:
				return nil, err
			}
			return result[0].Interface().([]byte), nil
		}, true
	},

	// SetterGetterRaw - output
	func(i interface{}) (func(Session, []byte) ([]byte, error), bool) {
		t := reflect.TypeOf(i)
		if t.NumIn() != 2 {
			return nil, false
		}
		if t.NumOut() != 2 {
			return nil, false
		}
		if !t.In(0).Implements(apiSessionType) {
			return nil, false
		}
		if t.Out(0).String() != "[]uint8" {
			return nil, false
		}
		if t.In(1).String() == "[]uint8" {
			return nil, false
		}
		if t.Out(1).String() != "error" {
			return nil, false
		}
		v := reflect.ValueOf(i)
		inputType := t.In(1)
		if inputType.Kind() == reflect.Ptr {
			inputType = inputType.Elem()
		}
		return func(session Session, msg []byte) ([]byte, error) {
			input := []reflect.Value{
				reflect.ValueOf(session),
				reflect.New(inputType),
			}
			err := json.Unmarshal(msg, input[1].Interface())
			if err != nil {
				return nil, err
			}

			// *int -> int, *string -> string, ...
			switch input[1].Interface().(type) {
			case *string, *int:
				input[1] = input[1].Elem()
			}

			result := v.Call(input)
			switch err := result[1].Interface().(type) {
			case error:
				return nil, err
			}
			return result[0].Interface().([]byte), nil
		}, true
	},

	// SetterGetterRaw - input
	func(i interface{}) (func(Session, []byte) ([]byte, error), bool) {
		t := reflect.TypeOf(i)
		if t.NumIn() != 2 {
			return nil, false
		}
		if t.NumOut() != 2 {
			return nil, false
		}
		if !t.In(0).Implements(apiSessionType) {
			return nil, false
		}
		if t.In(1).String() != "[]uint8" {
			return nil, false
		}
		if t.Out(0).String() == "[]uint8" {
			return nil, false
		}
		if t.Out(1).String() != "error" {
			return nil, false
		}
		v := reflect.ValueOf(i)
		return func(session Session, msg []byte) ([]byte, error) {
			input := []reflect.Value{
				reflect.ValueOf(session),
				reflect.ValueOf(msg),
			}
			result := v.Call(input)
			switch err := result[1].Interface().(type) {
			case error:
				return nil, err
			}
			return json.Marshal(result[0].Interface())
		}, true
	},

	// NoSetterNoGetter
	func(i interface{}) (func(Session, []byte) ([]byte, error), bool) {
		t := reflect.TypeOf(i)
		if t.NumIn() != 1 {
			return nil, false
		}
		if t.NumOut() != 1 {
			return nil, false
		}
		if !t.In(0).Implements(apiSessionType) {
			return nil, false
		}
		if t.Out(0).String() != "error" {
			return nil, false
		}
		v := reflect.ValueOf(i)
		return func(session Session, msg []byte) ([]byte, error) {
			result := v.Call([]reflect.Value{
				reflect.ValueOf(session),
			})
			switch err := result[0].Interface().(type) {
			case error:
				return nil, err
			}
			return nil, nil
		}, true
	},

	// SetterNoGetter
	func(i interface{}) (func(Session, []byte) ([]byte, error), bool) {
		t := reflect.TypeOf(i)
		if t.NumIn() != 2 {
			return nil, false
		}
		if t.NumOut() != 1 {
			return nil, false
		}
		if !t.In(0).Implements(apiSessionType) {
			return nil, false
		}
		if t.Out(0).String() != "error" {
			return nil, false
		}
		v := reflect.ValueOf(i)
		inputType := t.In(1)
		if inputType.Kind() == reflect.Ptr {
			inputType = inputType.Elem()
		}
		return func(session Session, msg []byte) ([]byte, error) {
			input := []reflect.Value{
				reflect.ValueOf(session),
				reflect.New(inputType),
			}
			err := json.Unmarshal(msg, input[1].Interface())
			if err != nil {
				return nil, err
			}

			// *int -> int, *string -> string, ...
			switch input[1].Interface().(type) {
			case *string, *int:
				input[1] = input[1].Elem()
			}

			result := v.Call(input)
			switch err := result[0].Interface().(type) {
			case error:
				return nil, err
			}
			return nil, nil
		}, true
	},

	// NoSetterGetter
	func(i interface{}) (func(Session, []byte) ([]byte, error), bool) {
		t := reflect.TypeOf(i)
		if t.NumIn() != 1 {
			return nil, false
		}
		if t.NumOut() != 2 {
			return nil, false
		}
		if !t.In(0).Implements(apiSessionType) {
			return nil, false
		}
		if t.Out(1).String() != "error" {
			return nil, false
		}
		v := reflect.ValueOf(i)
		return func(session Session, msg []byte) ([]byte, error) {
			result := v.Call([]reflect.Value{
				reflect.ValueOf(session),
			})
			switch err := result[1].Interface().(type) {
			case error:
				return nil, err
			}
			return json.Marshal(result[0].Interface())
		}, true
	},

	// SetterGetter
	func(i interface{}) (func(Session, []byte) ([]byte, error), bool) {
		t := reflect.TypeOf(i)
		if t.NumIn() != 2 {
			return nil, false
		}
		if t.NumOut() != 2 {
			return nil, false
		}
		if !t.In(0).Implements(apiSessionType) {
			return nil, false
		}
		if t.Out(1).String() != "error" {
			return nil, false
		}
		v := reflect.ValueOf(i)
		inputType := t.In(1)
		if inputType.Kind() == reflect.Ptr {
			inputType = inputType.Elem()
		}
		return func(session Session, msg []byte) ([]byte, error) {
			input := []reflect.Value{
				reflect.ValueOf(session),
				reflect.New(inputType),
			}
			err := json.Unmarshal(msg, input[1].Interface())
			if err != nil {
				return nil, err
			}

			// *int -> int, *string -> string, ...
			switch input[1].Interface().(type) {
			case *string, *int:
				input[1] = input[1].Elem()
			}

			result := v.Call(input)
			switch err := result[1].Interface().(type) {
			case error:
				return nil, err
			}
			return json.Marshal(result[0].Interface())
		}, true
	},
}
