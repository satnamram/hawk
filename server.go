package hawk

import (
	"encoding/json"
	"errors"
	"io"
	"sync"
	"bytes"
)

type Server struct {
	api *serverAPI

	sessions      []*serverSession
	sessionsWG    sync.WaitGroup
	sessionsMutex sync.Mutex

	onDisconnect func(info *SessionInfo, err error)
}

type SessionInfo struct {
	Addr string
	Name string
	Meta map[string]interface{}
}

func NewServer(v interface{}) (*Server, error) {
	api, err := newServerAPI(v)
	if err != nil {
		return nil, err
	}
	return &Server{
		api:      api,
		sessions: []*serverSession{},
	}, nil
}

func (server *Server) Register(moduleName string, v interface{}) error {
	return server.api.Register(moduleName, v)
}

func (server *Server) OnDisconnect(f func(info *SessionInfo, err error)) {
	server.onDisconnect = f
}


func (server *Server) Serve(info *SessionInfo, conn io.ReadWriteCloser) error {
	session := &serverSession{
		info:   info,
		socket: NewSocket(conn),
	}

	// add session to session hub
	server.sessionsMutex.Lock()
	server.sessions = append(server.sessions, session)
	server.sessionsMutex.Unlock()

	// create serve routine for session
	server.sessionsWG.Add(1)

	// error value for the disconnect handler
	var err error

	// remove session from session hub
	defer func() {
		server.sessionsMutex.Lock()
		for i, existingSession := range server.sessions {
			if existingSession == session {
				server.sessions = append(server.sessions[:i], server.sessions[i+1:]...)
				break
			}
		}
		server.sessionsMutex.Unlock()
		if server.onDisconnect != nil {
			server.onDisconnect(info, err)
		}
		server.sessionsWG.Done()
	}()

	// handle mesages
	for {

		// read message from socket
		var msg *Message
		msg, err = session.socket.Read()
		if err != nil {
			return err // kill Connection
		}

		// check type of incoming message
		if !bytes.Equal(msg.Type,  Request) {
			err = errors.New("wrong message type")
			return err // kill Connection
		}

		// get method if exist
		if f, exist := server.api.Methods[msg.Command]; exist {

			// method found - execute in new routine
			go func() {
				ret, execErr := f(session, msg.Data)

				// respond with error message if call failed
				if execErr != nil {
					msg.Command = ""
					msg.Type = Error
					data, err := json.Marshal(execErr.Error())
					if err != nil {
						data = []byte("error marshalling error")
					}
					msg.Data = data
					session.socket.Write(msg)
					return
				}

				// respond with return value if call succeeded
				msg.Command = ""
				msg.Type = Success
				msg.Data = ret
				session.socket.Write(msg)
			}()

			continue // OK
		}

		// command not found
		msg.Command = ""
		msg.Type = Error
		msg.Data = []byte("\"command not found\"")
		session.socket.Write(msg)
	}
}
